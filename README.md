# Docker sandbox

```shell
git clone
```
## Compose with args validation in bash

```shell
cd valid_args
```

<details>

<summary>Fail validation</summary>

```shell
/bin/bash hehe.sh SUBDIRECTORY=bob GIBSON=sg FILE_NAME=output.txt
```

```txt
>>> *** MANDATORY ARGS VALIDATION ***

>>> Mandatory arg SUBDIRECTORY provided with valid value < bob >

>>> Mandatory arg FILE_NAME provided with valid value < output.txt >

>>> Arg COMMON_MOUNTED is empty, please provide it
An error occurred. Exiting..
```
</details>


<details>

<summary>Success validation</summary>

```sh
/bin/bash hehe.sh GIBSON=sg PARAM=neelix SUBDIRECTORY=bob FILE_NAME=airbourne.py COMMON_MOUNTED="/opt/sandbox/314159265"
```

```txt
>>> *** MANDATORY ARGS VALIDATION ***

>>> Mandatory arg SUBDIRECTORY provided with valid value < bob >

>>> Mandatory arg FILE_NAME provided with valid value < airbourne.py >

>>> Mandatory arg COMMON_MOUNTED provided with valid value < /opt/sandbox/314159265 >

>>> *** OPTIONNAL ARGS VALIDATION ***

>>> Optional arg GIBSON provided with valid value < sg >

>>> Optional arg PARAM provided with valid value < neelix >

>>> Optional args EXTERNAL_PARAM is empty, default value in source code will be used

>>> Building both docker app

# docker build
# ...
# ...
# ...


>>> Running 1st docker
root     - INFO     - main           -> process           : Starting creation process
root     - INFO     - main           -> process           : Creating /opt/app/data/bob
root     - INFO     - main           -> process           : Writing neelix in airbourne.py
[■■■■■■■■■■]waiting container 1
>>> Running 2nd docker
root     - INFO     - main           -> process           : Starting usage process
root     - INFO     - main           -> process           : Reading in /opt/app/data/bob/airbourne.py
root     - INFO     - main           -> process           : New data : < NEELIX >
0
U can find ur result in /opt/sandbox/314159265/bob/airbourne.py 
```
</details>


<details>

<summary>Schema</summary>

```mermaid

flowchart TB
    subgraph Host

        mounted["mounted volume"]

        subgraph Container1
            create["create_data()"]
        end

        subgraph Container2
            use["use_data()"]
        end

        create --> mounted
        mounted --> use
    end
```

</details>

Todo : 

- [ ] For args in mandatory concat -e
- [ ] For args in optional (provided) concat -e
- [X] PARAM not provided (remove generic -e)
- [ ] Unknown arg