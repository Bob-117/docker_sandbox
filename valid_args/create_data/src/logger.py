import logging


def init(_name='root'):
    log_format = logging.Formatter('%(name)-8s - %(levelname)-8s - %(module)-25s -> %(funcName)-38s: %(message)s')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_format)

    _log = logging.getLogger()
    _log.name = _name
    _log.setLevel(logging.INFO)
    logging.getLogger('urllib3').setLevel(logging.INFO)
    logging.getLogger('matplotlib.font_manager').setLevel(logging.INFO)
    logging.getLogger('PIL.PngImagePlugin').setLevel(logging.INFO)
    logging.getLogger().addHandler(stream_handler)

    # for key in logging.Logger.manager.loggerDict:
    #     print(key)
