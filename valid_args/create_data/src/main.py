"""
Sandbox service creating some data
"""

# Standart
import logging
import os
import sys
import pathlib
from time import sleep

# Custom
import logger

_BASE = '/opt/sandbox'  # make sure you have access


def process():
    """
    Main process
    :return:
    """
    logger.init()
    logging.info('Starting creation process')
    
    directory = os.getenv('DIRECTORY', os.path.join(_BASE, 'docker/local'))
    subdirectory = os.getenv('SUBDIRECTORY', 'data')
    output_file_name = os.getenv('FILE_NAME', 'result.txt')

    extern_param = os.getenv('PARAM', 'default_value')

    full_path = os.path.join(directory, subdirectory)

    d = pathlib.Path(full_path)
    logging.error(f"=============== {d} - {d.exists()}")
    if not d.exists():
        logging.info(f'Creating {full_path}')
        logging.error(f'Creating {full_path}')
        d.mkdir(parents=True)

    with open(os.path.join(full_path, output_file_name), mode='w', encoding='utf-8') as output_file:
        logging.info(f'Writing {extern_param} in {output_file_name}')


        ##
        loading_parts = ["[■□□□□□□□□□]", "[■■□□□□□□□□]", "[■■■□□□□□□□]", "[■■■■□□□□□□]", "[■■■■■□□□□□]",
                     "[■■■■■■□□□□]", "[■■■■■■■□□□]", "[■■■■■■■■□□]", "[■■■■■■■■■□]", "[■■■■■■■■■■]"]
        count = len(loading_parts)
        for i in range(count):
            sleep(.2)
            sys.stdout.write("\r" + loading_parts[i % count])
            sys.stdout.flush()

        ##
        output_file.write(extern_param)


process()
