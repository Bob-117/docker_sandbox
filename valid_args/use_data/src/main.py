"""
Sandbox service creating some data
"""
import logging
import os
import pathlib

import logger

_BASE = '/opt/app'  # make sure you have access


def process():
    """
    Main process
    :return:
    """
    logger.init()
    logging.info('Starting usage process')
    directory = os.getenv('DIRECTORY', os.path.join(_BASE, 'docker_after_docker/local'))
    subdirectory = os.getenv('SUBDIRECTORY', 'data')
    data_file_name = os.getenv('FILE_NAME', 'output')

    full_path = os.path.join(directory, subdirectory, data_file_name)

    if not os.path.isfile(full_path):
        logging.error(f'Cant find data in {full_path}')
        exit()

    with open(full_path, mode='r') as output_file:
        logging.info(f'Reading in {full_path}')
        _data = output_file.read()
    
    new_data = f'< {_data.upper()} >'

    logging.info(f'New data : {new_data}')


process()
