#!/bin/bash

for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    VALUE=$(echo "$ARGUMENT" | cut -f2 -d=)
    export "$KEY"="$VALUE"
done

logger() {
  echo ">>> $@"
}

missing_mandatory() {
    logger "Arg $1 is empty, please provide it"
    return 1
}

missing_optional() {
    logger "Optional args $1 is empty, default value in source code will be used"
    return 0 
    # e.g., _port = os.getenv('PORT', 5000)
}

validate_mandatory() {
    logger "Mandatory arg $1 provided with valid value < $2 >"
    return 0
}

validate_optional() {
    logger "Optional arg $1 provided with valid value < $2 >"
    return 0
}

validate_args() {
    #
    # $1 associative array 
    # declare -A arr=([VAR]="$VAR")
    #
    # $2 missing_check method name
    # what to do in case of not provided arg
    #
    # $3 validation_method name
    # what to do with a provided arg
    #

    local -n array="$1" 
    local missing_check="$2"
    local validate="$3"
    local result=0

    for _arg in "${!array[@]}"; do
        # echo "- $_arg"
        # echo "Key: $_arg"
        # echo "Value: ${array[$_arg]}"

        if [[ -z ${array[$_arg]} ]]; then
            $missing_check $_arg
            result=$?
        else
            $validate $_arg ${array[$_arg]}
            result=$?
        fi
        # echo "RESULT: $result $_arg"
        if [[ $result -eq 1 ]]; then
            echo "An error occurred. Exiting.."
            exit 1
        fi
        echo
    done

    
}

declare -A mandatory=(
    # [DIRECTORY]="$DIRECTORY"
    [SUBDIRECTORY]="$SUBDIRECTORY"
    [FILE_NAME]="$FILE_NAME"
    [COMMON_MOUNTED]="$COMMON_MOUNTED"
)

declare -A optional=(
    [EXTERNAL_PARAM]="$EXTERNAL_PARAM"
    [PARAM]="$PARAM"
    [GIBSON]="$GIBSON"
)


build_both_docker()
{
  logger "Building both docker app"
  docker rm CONTAINER1 CONTAINER2
  cd create_data || return
  docker build --rm --no-cache -t create_data:1.0.0 .
  cd ../use_data || return
  docker build --rm --no-cache -t use_data:1.0.0 .
  printf $"\n\n"
}

run_one_docker_after_another()
{

    logger "Running 1st docker"
    # for arg in optional: += -e ARG="$ARG"

    if [[ -z $PARAM ]]; then
            docker run -e DIRECTORY='/opt/app/data' -e SUBDIRECTORY="$SUBDIRECTORY" -e FILE_NAME="$FILE_NAME" -v "$COMMON_MOUNTED":'/opt/app/data' --name CONTAINER1 create_data:1.0.0

        else
            docker run -e DIRECTORY='/opt/app/data' -e PARAM="$PARAM" -e SUBDIRECTORY="$SUBDIRECTORY" -e FILE_NAME="$FILE_NAME" -v "$COMMON_MOUNTED":'/opt/app/data' --name CONTAINER1 create_data:1.0.0
        fi


    docker wait CONTAINER1 >/dev/null ; echo "waiting container 1"
    logger "Running 2nd docker"
    docker run -e DIRECTORY='/opt/app/data' -e SUBDIRECTORY="$SUBDIRECTORY" -e FILE_NAME="$FILE_NAME" -v "$COMMON_MOUNTED":'/opt/app/data'  --name CONTAINER2 use_data:1.0.0
    docker wait CONTAINER2 
    printf "U can find ur result in $COMMON_MOUNTED/$SUBDIRECTORY/$FILE_NAME "
}

process()
{
    logger "*** MANDATORY ARGS VALIDATION ***"
    echo
    validate_args mandatory missing_mandatory validate_mandatory
    logger "*** OPTIONNAL ARGS VALIDATION ***"
    echo
    validate_args optional missing_optional validate_optional

    build_both_docker
    run_one_docker_after_another
}

process

# /bin/bash hehe.sh DIRECTORY=bob GIBSON=sg